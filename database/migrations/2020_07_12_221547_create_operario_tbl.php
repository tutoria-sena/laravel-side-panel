<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperarioTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operario_tbl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->length(20);
            $table->string('apellido')->length(20);
            $table->bigInteger('documento')->length(12);
            $table->bigInteger('celular')->length(13);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operario_tbl');
    }
}
