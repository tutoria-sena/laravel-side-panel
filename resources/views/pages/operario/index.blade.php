@extends('layouts.myapp')

@section('my-content')
<div class="p-operarios">
  <div class="p-operarios__container">
    <table class="p-operarios__list table">
      <thead class="thead-dark">
        <tr>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Documento</th>
          <th>Celular</th>
        </tr>
      </thead>
      <tbody>
        @foreach($data as $key => $value)
          <tr>
            <td>{{ $value->nombre }}</td>
            <td>{{ $value->apellido }}</td>
            <td>{{ $value->documento }}</td>
            <td>{{ $value->celular }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection