<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Welcom Test ModuleS</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        <section class="p-welcome">
            <div class="app-container d-flex justify-content-center align-items-center">
                <div class="p-welcome__content">
                    <h1 class="p-welcome__title">Test Module App</h1>
                    @if (Route::has('login'))
                        <div class="p-welcome__links">
                            @auth
                                <a class="btn btn-primary" href="{{ url('/home') }}">Home</a>
                            @else
                                <a class="btn btn-primary" href="{{ route('login') }}">Login</a>

                                @if (Route::has('register'))
                                    <a class="btn btn-primary" href="{{ route('register') }}">Register</a>
                                @endif
                            @endauth
                        </div>
                    @endif
                </div>
            </div>
        </section>
    </body>
</html>
