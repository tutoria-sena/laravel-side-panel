let $sidePanelNav = $(".o-side-panel-nav");
let $sidePanelOpenIcon = $(".o-side-panel__open-menu-icon");
let $sidePanelCloseIcon = $(".o-side-panel-nav__close-menu-icon");

$(document).on('click',function() {
    if ($sidePanelNav.hasClass('is-open')) {
        $sidePanelNav.removeClass('is-open');
    }
});

$sidePanelNav.click(function(event) {
    event.stopPropagation();
});

$sidePanelOpenIcon.click(function(event) {
    event.stopPropagation();
    $sidePanelNav.addClass('is-open');
});

$sidePanelCloseIcon.click(function(event) {
    event.stopPropagation();
    $sidePanelNav.removeClass('is-open');
});
  