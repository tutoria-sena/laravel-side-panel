<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Operario extends Model
{

    protected $table='operario_tbl';

    protected $fillable = [
       'id', 
       'nombre', 
       'apellido',
       'documento',
       'celular',
    ];
}
