<?php

namespace App\Http\Controllers\Operario;

use App\Http\Models\Operario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OperarioController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mensaje='';
        $data = Operario::paginate(6);
        return view('pages.operario.index')->with('data', $data)->with('mensaje', $mensaje);
    }
}
